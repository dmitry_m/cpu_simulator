# README #

CPU emulator. 2014.

Example:

		word null				0
		word one				1
		word counter			5
		word increment			1
		word value
		
		str  endl				"\n"
		str  in_f				"in function\n"			
		str  out_f				"out of function\n"	
		str  s_f1				"after function 1\n"
		str  s_f2				"after function 2\n"
		str  s_f3				"after function 3\n"

	function newLine
		txt endl
		ret

	function foo
		txt in_f
		pop value
	:	beg
		out counter
		call newLine

		sub counter one
		cmp counter null
		jne beg
		
		call newLine

		txt out_f	

		ret
		
	// -------------------------------------------------
	:	start
		txt in_f
		txt "hello"

		push 3 
		call foo
		txt s_f1	

		sub counter one
		push counter
		call foo	
		txt s_f2

		sub counter one
		push counter
		call foo	
		txt s_f3

		ret
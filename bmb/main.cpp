#include "ide/ide.h"

using namespace bmb;

int main() {
	Ide ide;
	ide.start();

	return 0;
}
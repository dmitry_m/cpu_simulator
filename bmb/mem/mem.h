#ifndef MEM_H_
#define MEM_H_

#include <string>
#include "constants/spec.h"
#include "common/electrical.h"
#include "mem/valueDescriptor.h"
#include <fstream>

namespace bmb {

class Mem : public Electrical {
public:
	Mem(const std::string&);

	void load() {
		std::ifstream in(m_memoryImage + BINARY_FILE_EXT);
		Byte * offset = m_ram;
		while (in) {
			in.read(offset++, 1);
		}
		in.close();
	}

	template< class DataTy >
	DataTy realAddress(Word addr) {
		return static_cast<DataTy>(m_ram + addr);
	}

	template< class DataTy >
	ValueDescriptor<DataTy> descriptor(Word addr, bool readonly = false) {
		return ValueDescriptor<DataTy>(m_ram, addr, readonly);
	}

	template< class DataTy >
	DataTy get(Word addr) {
		return descriptor<DataTy>(addr, false).get<DataTy>();
	}

	template< class DataTy >
	void set(Word addr, const DataTy & val) {
		descriptor<DataTy>(addr, false).set<DataTy>(val);
	}

	template< class DataTy >
	void push(Word &addr, DataTy val) {
		addr -= sizeof(DataTy);
		set(addr, val);		
	}

	template< class DataTy >
	void pop(Word &addr, DataTy & val) {
		val = get<DataTy>(addr);
		addr += sizeof(DataTy);
	}

private:
	Byte m_ram[RamSize];
	std::string m_memoryImage;
};

} // namespace bmb

#endif
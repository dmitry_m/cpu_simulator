#ifndef VALUEDESCRIPTOR_H_
#define VALUEDESCRIPTOR_H_

#include <string>
#include <algorithm>
#include <type_traits>
#include "constants/valueTraits.h"

namespace bmb {

template< class DataTy >
class ValueDescriptor {
public:
	typedef DataTy					dataType;
	typedef ValueTraits<DataTy>		traits;

	explicit ValueDescriptor(Byte * memoryAddress, Word dataOffset, bool readonly = false) 
		: m_memoryAddress(memoryAddress), m_dataOffset(dataOffset), m_readonly(readonly) {
	}

	inline int size() const {
		return (*(static_cast<Word*>(realAddress<Word*>()) - 1));
	}

	inline Word address() const {
		return m_dataOffset;
	}

	template< class DataTy >
	DataTy get() const {
		return (*reinterpret_cast<const DataTy*>(realAddress()));
	}

	template<>
	std::string get<std::string>() const {
		return std::string(realAddress<const char*>(), (std::string::size_type)size());
	}

	dataType get() const {
		return get<dataType>();
	}

	template< class DataTy >
	void set(const DataTy & value) {
		if (m_readonly) {
			throw BmbException("set(): trying to write when readonly");
		}

		(*static_cast<DataTy*>(realAddress())) = value;
	}

	template<>
	void set<std::string>(const std::string & value) {
		if (m_readonly) {
			throw BmbException("set(): trying to write when readonly");
		}

		memcpy(realAddress(), value.c_str(), (value.length() < size() ? value.length() : size()));
	}

	void set(const dataType & value) {
		set<dataType>(value);
	}

	operator DataTy() const {
		return get<DataTy>();
	}

	const ValueDescriptor<DataTy> & operator =(const ValueDescriptor<DataTy> & other) {
		set<DataTy>(other.get<DataTy>());
		return (*this);
	}

	const ValueDescriptor<DataTy> & operator =(const DataTy & other) {
		set<DataTy>(other);
		return (*this);
	}

private:
	template< class PointerTy >
	inline const PointerTy realAddress() const {
		return static_cast<PointerTy>(static_cast<void*>(m_memoryAddress + m_dataOffset));
	}

	template< class PointerTy >
	inline PointerTy realAddress() {
		return static_cast<PointerTy>(static_cast<void*>(m_memoryAddress + m_dataOffset));
	}

	inline dataType * realAddress() {
		return realAddress<dataType*>();
	}

	inline const dataType * realAddress() const {
		return realAddress<dataType*>();
	}

private:
	Byte *	m_memoryAddress;
	Word	m_dataOffset;
	bool	m_readonly;
};

} // namespace bmb

#endif
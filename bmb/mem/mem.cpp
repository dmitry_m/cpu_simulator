//#include <memory.h>
#include "mem.h"

namespace bmb {

Mem::Mem(const std::string &memoryImage)
	: m_memoryImage(memoryImage) {
	memset(m_ram, 0, sizeof(m_ram));
}

} // namespace bmb
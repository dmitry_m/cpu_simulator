#include "machine.h"

namespace bmb {

Machine::Machine(const std::string & filename, bool debug)
	: m_mem(filename),
	m_cpu(m_mem, debug),
	m_bootloader(m_cpu, m_mem) {
}

void Machine::powerOn() {
	m_cpu.powerOn();
	m_mem.powerOn();

	m_bootloader.boot();
}


void Machine::powerOff() {
	m_cpu.powerOff();
	m_mem.powerOff();
}

} // namespace bmb
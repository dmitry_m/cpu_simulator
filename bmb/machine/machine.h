#ifndef MACHINE_H_
#define MACHINE_H_

#include <string>
#include "cpu/cpu.h"
#include "mem/mem.h"
#include "bootloader/bootloader.h"
#include "common/electrical.h"

namespace bmb {

class Machine : public Electrical {
public:
	Machine(const std::string & filename, bool debug);

	void powerOn();

	void powerOff();

private:
	Mem				m_mem;
	Cpu				m_cpu;
	Bootloader		m_bootloader;
};

} // namespace bmb

#endif
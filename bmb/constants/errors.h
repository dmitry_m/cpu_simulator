#ifndef ERRORS_H_
#define ERRORS_H_

#include <string>
#include "constants\spec.h"

#define COMPILATION_ERROR					"compilation error!"
#define UNDEFINED_COMMAND					"undefined command"

namespace bmb {

class BmbException : public std::exception {
public:
	BmbException(const char * str)
		: exception(str) {
	}
};

template< class DataTy >
inline std::string getDataName() {
	return "Error determining type";
}

template<>
inline std::string getDataName<Byte>() {
	return "Byte";
}

template<>
inline std::string getDataName<Word>() {
	return "Word";
}

template< class DataTy >
class DataError {
public:
	static std::string notDefined() {
		return std::string() + "error 100: \'" + getDataName<DataTy>() + "\' not defined";
	}

	static std::string alreadyDefined() {
		return std::string() + "error 101: \'" + getDataName<DataTy>() + "\' already defined";
	}

	virtual ~DataError() final = 0;
};

} // namespace bmb

#endif
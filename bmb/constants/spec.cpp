#include "spec.h"

namespace bmb {

const char * code2caption(Byte byte) { 
	switch (byte) {
	LINK_CODE_AND_CAPTION(CODE_SHUTDOWN, CAPTION_SHUTDOWN)
	LINK_CODE_AND_CAPTION(CODE_ERROR, CAPTION_ERROR)

	LINK_CODE_AND_CAPTION(CODE_MOVE8, CAPTION_MOVE8)
	LINK_CODE_AND_CAPTION(CODE_MOVE16, CAPTION_MOVE16)
	LINK_CODE_AND_CAPTION(CODE_MOVE_OFF, CAPTION_MOVE_OFF)
	LINK_CODE_AND_CAPTION(CODE_MOVE_OFFS, CAPTION_MOVE_OFFS)
	LINK_CODE_AND_CAPTION(CODE_SEND8, CAPTION_SEND8)
	LINK_CODE_AND_CAPTION(CODE_SEND16, CAPTION_SEND16)

	LINK_CODE_AND_CAPTION(CODE_ADD8, CAPTION_ADD8)
	LINK_CODE_AND_CAPTION(CODE_ADD16, CAPTION_ADD16)
	LINK_CODE_AND_CAPTION(CODE_SUB8, CAPTION_SUB8)
	LINK_CODE_AND_CAPTION(CODE_SUB16, CAPTION_SUB16)

	LINK_CODE_AND_CAPTION(CODE_CMP8, CAPTION_CMP8)
	LINK_CODE_AND_CAPTION(CODE_CMP16, CAPTION_CMP16)
	LINK_CODE_AND_CAPTION(CODE_JMP, CAPTION_JMP)
	LINK_CODE_AND_CAPTION(CODE_JE, CAPTION_JE)
	LINK_CODE_AND_CAPTION(CODE_JNE, CAPTION_JNE)
	LINK_CODE_AND_CAPTION(CODE_JG, CAPTION_JG)
	LINK_CODE_AND_CAPTION(CODE_JL, CAPTION_JL)

	LINK_CODE_AND_CAPTION(CODE_PUSH, CAPTION_PUSH)
	LINK_CODE_AND_CAPTION(CODE_POP, CAPTION_POP)
	LINK_CODE_AND_CAPTION(CODE_CALL, CAPTION_CALL)
	LINK_CODE_AND_CAPTION(CODE_RET, CAPTION_RET)

	LINK_CODE_AND_CAPTION(CODE_OUT8, CAPTION_OUT8)
	LINK_CODE_AND_CAPTION(CODE_OUT16, CAPTION_OUT16)
	LINK_CODE_AND_CAPTION(CODE_OUTS, CAPTION_OUTS)
	LINK_CODE_AND_CAPTION(CODE_OUTS_S, CAPTION_OUTS_S)
	LINK_CODE_AND_CAPTION(CODE_IN8, CAPTION_IN8)
	LINK_CODE_AND_CAPTION(CODE_IN16, CAPTION_IN16)
	default:
		return "undefined";
	}
}

} // namespace bmb

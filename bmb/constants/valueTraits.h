#ifndef VALUETRAITS_H_
#define VALUETRAITS_H_

#include <string>

namespace bmb {

class Unknown {};

class ExplicitSize {};
class NotExplicitSize {};

template< class DataTy >
class ValueTraits {
public:
	typedef NotExplicitSize sizeType;
};

template<>
class ValueTraits<std::string> {
public:
	typedef ExplicitSize sizeType;
};

} // namespace bmb

#endif
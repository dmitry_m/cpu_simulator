#ifndef SPEC_H_
#define SPEC_H_


// captions
#define CAPTION_SHUTDOWN				"shd"
#define CAPTION_ERROR					"err"

#define CAPTION_MOVE8					"mov8"
#define CAPTION_MOVE16					"mov"
#define CAPTION_MOVE_OFF				"adrs"
#define CAPTION_MOVE_OFFS				"adr"
#define CAPTION_SEND8					"snd8"
#define CAPTION_SEND16					"snd"

#define CAPTION_ADD8					"add8"
#define CAPTION_ADD16					"add"
#define CAPTION_SUB8					"sub8"
#define CAPTION_SUB16					"sub"

#define CAPTION_CMP8					"cmp8"
#define CAPTION_CMP16					"cmp"
#define CAPTION_JMP						"jmp"
#define CAPTION_JE						"je"
#define CAPTION_JNE						"jne"
#define CAPTION_JG						"jg"
#define CAPTION_JL						"jl"

#define CAPTION_PUSH					"push"
#define CAPTION_POP						"pop"
#define CAPTION_FUNCTION				"function"
#define CAPTION_CALL					"call"
#define CAPTION_RET						"ret"

#define CAPTION_OUT8					"out8"
#define CAPTION_OUT16					"out"
#define CAPTION_OUTS					"txt"
#define CAPTION_OUTS_S					"txts"
#define CAPTION_IN8						"in8"
#define CAPTION_IN16					"in"

#define CAPTION_MARK					":"
#define CAPTION_BYTE					"byte"
#define CAPTION_WORD					"word"
#define CAPTION_STRING					"str"

// codes
#define CODE_SHUTDOWN					(Byte(0x0))
#define CODE_ERROR						(Byte(0x5))

#define CODE_MOVE8						(Byte(0x10))
#define CODE_MOVE16						(Byte(0x11))
#define CODE_MOVE_OFF					(Byte(0x15))
#define CODE_MOVE_OFFS					(Byte(0x16))
#define CODE_SEND8						(Byte(0x1B))
#define CODE_SEND16						(Byte(0x1C))

#define CODE_ADD8						(Byte(0x20))
#define CODE_ADD16						(Byte(0x21))
#define CODE_SUB8						(Byte(0x25))
#define CODE_SUB16						(Byte(0x26))

#define CODE_CMP8						(Byte(0x2E))
#define CODE_CMP16						(Byte(0x2F))
#define CODE_JMP						(Byte(0x30))
#define CODE_JE							(Byte(0x31))
#define CODE_JNE						(Byte(0x33))
#define CODE_JG							(Byte(0x35))
#define CODE_JL							(Byte(0x37))

#define CODE_PUSH						(Byte(0x50))
#define CODE_POP						(Byte(0x55))
#define CODE_CALL						(Byte(0x5A))
#define CODE_RET						(Byte(0x60))

#define CODE_OUT8						(Byte(0xA0))
#define CODE_OUT16						(Byte(0xA1))
#define CODE_OUTS						(Byte(0xA3))
#define CODE_OUTS_S						(Byte(0xA4))
#define CODE_IN8						(Byte(0xAA))
#define CODE_IN16						(Byte(0xAB))

#define CODE_VALUE						(Byte(0xE0))
#define CODE_OFFSET						(Byte(0xE1))
#define CODE_DEBUG						(Byte(0xDE))

#define SOURCE_FILE_EXT					".src"
#define BINARY_FILE_EXT					".bin"

namespace bmb {

typedef short int						Word;
typedef char							Byte;

union _regWithDoubleAccess {
	Word f;
	Byte h;
};

typedef Word					Reg;	// 16-bit
typedef _regWithDoubleAccess	RegS;	// 8-bit

enum {
	RamSize = 65536,
	StartAddress = 64,
	StartStackAddress = 65534
};



#define LINK_CODE_AND_CAPTION(code_value, code_caption) \
	case code_value##:\
		return code_caption;


const char * code2caption(Byte byte);

} // namespace bmb

#endif
#ifndef HANDLERS_H_
#define HANDLERS_H_

#include "cpu/cpu.h"
#include "cpu/predicates/arithmetic.h"
#include "mem/valueDescriptor.h"

namespace bmb {

/*
format:
	value:				<value>

	offset:				0x0F <value>

	value by offset:	*(<value>)

	smart value:		*(0x0F <value>) | <value> 
*/
	
class WorkerDebug {
public:
	static void work(Cpu & cpu) {
		cpu.getRvalue<Byte>();
	}
};

class WorkerError {
public:
	static void work(Cpu &) {
		std::cout << "error!" << std::endl;
	}
};

class WorkerShutdown {
public:
	static void work(Cpu & cpu) {
		cpu.powerOff();
	}
};

template< class DataTy >
class WorkerMove {
public:
	static void work(Cpu & cpu) {
		ValueDescriptor<DataTy> arg1 = cpu.getLvalue<DataTy>();
		ValueDescriptor<DataTy> arg2 = cpu.getRvalue<DataTy>();

		arg1 = arg2;
	}
};

class WorkerMoveOffset {
public:
	static void work(Cpu & cpu) {
		ValueDescriptor<Word> arg1 = cpu.getLvalue<Word>();
		ValueDescriptor<Word> arg2 = cpu.getLvalue<Word>();

		arg1 = arg2.address();
	}
};

template< class DataTy >
class WorkerSend {
public:
	static void work(Cpu & cpu) {
		ValueDescriptor<Word> arg1 = cpu.getLvalue<Word>();
		ValueDescriptor<DataTy> arg2 = cpu.getRvalue<DataTy>();

		cpu.memDescriptor<DataTy>(arg1) = arg2;
	}
};

template< class DataTy, class Predicate >
class WorkerArithmetic {
public:
	static void work(Cpu & cpu) {
		ValueDescriptor<DataTy> arg1 = cpu.getLvalue<DataTy>();
		ValueDescriptor<DataTy> arg2 = cpu.getRvalue<DataTy>();

		arg1 = Predicate::calc<DataTy>(arg1, arg2);
	}
};

template< class DataTy >
class WorkerCompare {
public:
	static void work(Cpu & cpu) {
		ValueDescriptor<DataTy> arg1 = cpu.getRvalue<DataTy>();
		ValueDescriptor<DataTy> arg2 = cpu.getRvalue<DataTy>();

		cpu.getReg().equal = (arg1 == arg2);
		cpu.getReg().greater = (arg1 > arg2);
	}
};

template< class JmpPredicateTy >
class WorkerJump {
public:
	static void work(Cpu & cpu) {
		Word arg = cpu.flowGetDestinationAddress();

		if (JmpPredicateTy(cpu.getReg()).jump()) {
			cpu.jump(arg);
		}
	}
};

class WorkerPush {
public:
	static void work(Cpu & cpu) {
		ValueDescriptor<Word> arg = cpu.getRvalue<Word>();

		cpu.push(arg);
	}
};

class WorkerPop {
public:
	static void work(Cpu & cpu) {
		Word arg = cpu.flowGetDestinationAddress();

		cpu.pop(arg);
	}
};

class WorkerCall {
public:
	static void work(Cpu & cpu) {
		Word arg = cpu.flowGetDestinationAddress();

		cpu.push(cpu.getReg().ip);
		cpu.jump(arg);
	}
};

class WorkerRet {
public:
	static void work(Cpu & cpu) {
		Word addr = 0;
		cpu.pop(addr);

		if (0 == addr) {
			WorkerShutdown::work(cpu);
		}

		cpu.jump(addr);
	}
};

template< class DataTy >
class WorkerOut {
public:
	static void work(Cpu & cpu) {
		ValueDescriptor<DataTy> arg = cpu.getRvalue<DataTy>();

		cpu.print<DataTy>(arg);
	}
};

class WorkerOutStringWithSize {
public:
	static void work(Cpu & cpu) {
		ValueDescriptor<std::string> arg1 = cpu.getRvalue<std::string>();
		ValueDescriptor<Word> arg2 = cpu.getRvalue<Word>();

		cpu.printSized(arg1, arg2);
	}
};

template< class DataTy >
class WorkerIn {
public:
	static void work(Cpu & cpu) {
		ValueDescriptor<DataTy> arg = cpu.getLvalue<DataTy>();

		DataTy value;
		std::cin >> value; 

		arg.set(value);
	}
};

} // namespace bmb


#endif
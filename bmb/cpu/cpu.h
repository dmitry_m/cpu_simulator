#ifndef CPU_H_
#define CPU_H_

#include <iostream>
#include <map>
#include <functional>

#include "constants/spec.h"
#include "constants/errors.h"
#include "mem/mem.h"
#include "registers.h"
#include "common/electrical.h"
#include "cpu/cpuresources.h"
#include "cpu/predicates/jmp.h"
#include "cpu/predicates/arithmetic.h"
#include "constants/valueTraits.h"

namespace bmb {

class Cpu : public Electrical {
public:
	Cpu(Mem &, bool debug);

	void start();

	Registers & getReg();
	
	void jump(Word addr) {
		m_reg.ip = addr;
	}

	void pop(ValueDescriptor<Word> dest) {
		Word val;
		pop(val);
		dest.set(val);
	}

	void push(const ValueDescriptor<Word> val) {
		push(val.get());
	}

	void pop(Word & val) {
		m_mem->pop<Word>(m_reg.sp, val);
	}

	void push(Word val) {
		m_mem->push<Word>(m_reg.sp, val);
	}
	
	template< class DataTy >
	ValueDescriptor<DataTy> getLvalue() {
		return flowGetValue<DataTy, false>();
	}

	template< class DataTy >
	ValueDescriptor<DataTy> getRvalue() {
		return flowGetValue<DataTy, true>();
	}

	template< class DataTy >
	void print(const ValueDescriptor<DataTy> & arg) {
		std::cout << arg.get();
	}

	template<>
	void print<Byte>(const ValueDescriptor<Byte> & arg) {
		std::cout << arg.get<Word>();
	}

	void printSized(ValueDescriptor<std::string> valueDescriptor, Word size) {
		std::cout << std::string(memGetRealAddress<char*>(valueDescriptor.address()), size);
	}

	// ----------------------------------------------------------

	/*
		Value
	
	
	*/
	
	Word flowGetDestinationAddress(bool readOnly = false) {
		Byte code = memRecv<Byte>();

		if (CODE_OFFSET == code) {
			return memRecv<Word>();
		}
		else if (CODE_VALUE == code) {
			if (readOnly) {
				int valueOffset = m_reg.ip;
				m_reg.ip += memRecv<Word>();
				return valueOffset;
			}
			else {
				throw BmbException("trying to change rvalue");
			}
		}
		else {
			throw BmbException("undefined data code");
		}
	}

	template< class DataTy, bool readOnly >
	ValueDescriptor<DataTy> flowGetValue() {
		Word destinationAddress = flowGetDestinationAddress(readOnly);
		ValueDescriptor<DataTy> valueDescriptor = memDescriptor<DataTy>(destinationAddress, readOnly);

		return valueDescriptor;
	}

	template< class DataTy >
	DataTy memGetRealAddress(Word address) {
		return m_mem->realAddress<DataTy>(address);
	}

	template< class DataTy >
	ValueDescriptor<DataTy> memDescriptor(Word addr, bool readonly = false) {
		return m_mem->descriptor<DataTy>(addr, readonly);
	}

	template< class DataTy >
	ValueDescriptor<DataTy> memDescriptor(const ValueDescriptor<Word> & pointer, bool readonly = false) {
		return m_mem->descriptor<DataTy>(pointer.get(), readonly);
	}

	template< class DataTy >
	DataTy memflowGetValue(Word addr) {
		return m_mem->get<DataTy>(addr);
	}

	template< class DataTy >
	void memSet(Word addr, const DataTy & val) {
		m_mem->set<DataTy>(addr, val);
	}

	template< class DataTy >
	DataTy memRecv() {
		DataTy returnValue = memflowGetValue<DataTy>(m_reg.ip);
		m_reg.ip += sizeof(DataTy);
		return returnValue;
	}

private:
	
private:

	void cycle();

	void initHandlers();
	
	void reset();

	// handlers
	template< class WorkStrategy >
	void CCommand();
	/*
	template< class WorkStrategy >
	void CCommandWith0Arguments();

	template< class ArgTy,
			  class WorkStrategy >
	void CCommandWith1Arguments();

	template< class Arg1Ty,
			  class Arg2Ty,	
			  class WorkStrategy >
	void CCommandWith2Arguments();
*/
private:

	std::map< Byte, std::function< void() > >	m_handler;

	Registers m_reg;
	Mem * m_mem;

	long m_instructionNumber;
	bool m_debug;
};

} // namespace bmb

#endif
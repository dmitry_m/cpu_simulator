#include <iostream>
#include "registers.h"
#include "cpu/handlers.h"
#include "cpu.h"

// cpu

namespace bmb {

Cpu::Cpu(Mem &mem, bool debug)
	: m_mem(&mem),
	  m_debug(debug) {
	initHandlers();
}

Registers & Cpu::getReg() {
	return m_reg;
}

void Cpu::reset() {
	m_instructionNumber = 0;

	m_reg.sp = StartStackAddress;
	m_reg.ip = StartAddress;
	push(Word(0));
}

void Cpu::start() {
	reset();

	while (m_power) {
		cycle();
	}
}

void Cpu::cycle() {
	Byte cmd = memRecv<Byte>();
	
	if (m_debug) {
		std::cout << "instruction " << m_instructionNumber << ": " << bmb::code2caption(cmd) << "(0x" << std::hex << (unsigned)cmd << ")" << std::endl;
	}

	m_instructionNumber++;

	return (m_handler[cmd]());
}

// handlers

void Cpu::initHandlers() {
	m_handler[CODE_SHUTDOWN]	= std::bind(&Cpu::CCommand<WorkerShutdown>, this);
	m_handler[CODE_ERROR]		= std::bind(&Cpu::CCommand<WorkerError>, this);

	m_handler[CODE_MOVE8]		= std::bind(&Cpu::CCommand< WorkerMove<Byte> >, this);
	m_handler[CODE_MOVE16]		= std::bind(&Cpu::CCommand< WorkerMove<Word> >, this);
	m_handler[CODE_MOVE_OFF]	= std::bind(&Cpu::CCommand< WorkerMoveOffset >, this);
	m_handler[CODE_SEND8]		= std::bind(&Cpu::CCommand< WorkerSend<Byte> >, this);
	m_handler[CODE_SEND16]		= std::bind(&Cpu::CCommand< WorkerSend<Word> >, this);

	m_handler[CODE_ADD8]		= std::bind(&Cpu::CCommand< WorkerArithmetic<Byte, predicates::arithmetic::Add> >, this);
	m_handler[CODE_ADD16]		= std::bind(&Cpu::CCommand< WorkerArithmetic<Word, predicates::arithmetic::Add> >, this);
	m_handler[CODE_SUB8]		= std::bind(&Cpu::CCommand< WorkerArithmetic<Byte, predicates::arithmetic::Sub> >, this);
	m_handler[CODE_SUB16]		= std::bind(&Cpu::CCommand< WorkerArithmetic<Word, predicates::arithmetic::Sub> >, this);

	m_handler[CODE_CMP8]		= std::bind(&Cpu::CCommand< WorkerCompare<Byte> >, this);
	m_handler[CODE_CMP16]		= std::bind(&Cpu::CCommand< WorkerCompare<Word> >, this);
	m_handler[CODE_JMP]			= std::bind(&Cpu::CCommand< WorkerJump<predicates::jmp::Jmp> >, this);
	m_handler[CODE_JE]			= std::bind(&Cpu::CCommand< WorkerJump<predicates::jmp::JmpIfEqual> >, this);
	m_handler[CODE_JNE]			= std::bind(&Cpu::CCommand< WorkerJump<predicates::jmp::JmpIfNotEqual> >, this);
	m_handler[CODE_JG]			= std::bind(&Cpu::CCommand< WorkerJump<predicates::jmp::JmpIfGreater> >, this);
	m_handler[CODE_JL]			= std::bind(&Cpu::CCommand< WorkerJump<predicates::jmp::JmpIfLess> >, this);

	m_handler[CODE_PUSH]		= std::bind(&Cpu::CCommand< WorkerPush >, this);
	m_handler[CODE_POP]			= std::bind(&Cpu::CCommand< WorkerPop >, this);
	m_handler[CODE_CALL]		= std::bind(&Cpu::CCommand< WorkerCall >, this);
	m_handler[CODE_RET]			= std::bind(&Cpu::CCommand< WorkerRet >, this);

	m_handler[CODE_OUT8]		= std::bind(&Cpu::CCommand< WorkerOut<Byte> >, this);
	m_handler[CODE_OUT16]		= std::bind(&Cpu::CCommand< WorkerOut<Word> >, this);
	m_handler[CODE_OUTS]		= std::bind(&Cpu::CCommand< WorkerOut<std::string> >, this);
	m_handler[CODE_OUTS_S]		= std::bind(&Cpu::CCommand< WorkerOutStringWithSize >, this);

	m_handler[CODE_IN8]			= std::bind(&Cpu::CCommand< WorkerIn<Byte> >, this);
	m_handler[CODE_IN16]		= std::bind(&Cpu::CCommand< WorkerIn<Word> >, this);

	m_handler[CODE_DEBUG]		= std::bind(&Cpu::CCommand< WorkerDebug >, this);
}

/////////////////////////////////////////////////////////////////////////////////////////
// common handlers 

template< class WorkStrategy >
void Cpu::CCommand() {
	WorkStrategy::work(*this);
}

/*

template< class WorkStrategy >
void Cpu::CCommandWith0Arguments() {
	WorkStrategy::work(*this);
}

template< class ArgTy,
		  class WorkStrategy >
void Cpu::CCommandWith1Arguments() {

	WorkStrategy::work(*this, arg);
}

template< class ArgTy,
		  class WorkStrategy >
void Cpu::CCommandWith2Arguments<ArgTy, ArgTy, WorkStrategy>() {
	CCommandWith2Arguments<ArgTy, ArgTy, WorkStrategy>();
}

template< class Arg1Ty,
		  class Arg2Ty,
		  class WorkStrategy >
void Cpu::CCommandWith2Arguments() {
	ValueDescriptor<Arg1Ty> arg1 = cpu.getRvalue<Arg1Ty>::take(*this);
	ValueDescriptor<Arg2Ty> arg2 = cpu.getRvalue<Arg2Ty>::take(*this);

	WorkStrategy<Arg1Ty, Arg2Ty>::work(*this, arg1, arg2);
}*/

} // namespace bmb
#ifndef REGISTERS_H_
#define REGISTERS_H_

#include "constants/spec.h"

namespace bmb {

struct Registers {
	RegS ra;
	RegS rb;
	RegS rc;
	RegS rd;

	Reg ip;
	Reg sp;
	Reg bp;

	// flags
	bool zero;
	bool equal;
	bool greater;
};

} // namespace bmb

#endif
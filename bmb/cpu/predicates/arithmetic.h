#ifndef ARITHMETIC_H_
#define ARITHMETIC_H_

#include "cpu\registers.h"

namespace bmb {
namespace predicates {
namespace arithmetic {

class Add {
public:
	template< class DataTy >
	static DataTy calc(const DataTy & lhs, const DataTy & rhs) {
		return lhs + rhs;
	}
};

class Sub {
public:
	template< class DataTy >
	static DataTy calc(const DataTy & lhs, const DataTy & rhs) {
		return lhs - rhs;
	}
};

class Mul {
public:
	template< class DataTy >
	static DataTy calc(const DataTy & lhs, const DataTy & rhs) {
		return lhs * rhs;
	}
};

class Div {
public:
	template< class DataTy >
	static DataTy calc(const DataTy & lhs, const DataTy & rhs) {
		return lhs / rhs;
	}
};

} // namespace arithmetic
} // namespace predicates
} // namespace bmb

#endif


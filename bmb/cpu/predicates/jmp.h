#ifndef JMP_H_
#define JMP_H_

#include "cpu\registers.h"

namespace bmb {
namespace predicates {
namespace jmp {

class Jmp {
public:
	Jmp(const Registers & regs)
		: m_regs(regs) {
	}

	bool jump() {
		return true;
	}

	const Registers & getRegisters() {
		return m_regs;
	}

private:
	const Registers & m_regs;
};

class JmpIfEqual : public Jmp {
public:
	JmpIfEqual(const Registers & regs)
		: Jmp(regs) {
	}

	bool jump() {
		return (getRegisters().equal);
	}
};

class JmpIfNotEqual : public Jmp {
public:
	JmpIfNotEqual(const Registers & regs)
		: Jmp(regs) {
	}

	bool jump() {
		return (!getRegisters().equal);
	}
};

class JmpIfGreater : public Jmp {
public:
	JmpIfGreater(const Registers & regs)
		: Jmp(regs) {
	}

	bool jump() {
		return (getRegisters().greater);
	}
};

class JmpIfLess : public Jmp {
public:
	JmpIfLess(const Registers & regs)
		: Jmp(regs) {
	}

	bool jump() {
		return (!getRegisters().greater && !getRegisters().equal);
	}
};

} // namespace jmp
} // namespace predicates
} // namespace bmb

#endif


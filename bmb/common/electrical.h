#ifndef ELECTRICAL_H_
#define ELECTRICAL_H_

namespace bmb {

class Electrical {
public:
	Electrical()
		: m_power(false) {
	}

	virtual void powerOn() {
		m_power = true;
	}

	virtual void powerOff() {
		m_power = false;
	}

protected:
	bool m_power;
};

} // namespace bmb

#endif
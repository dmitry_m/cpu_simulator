#ifndef IDE_H_
#define IDE_H_

#include <string>
#include <boost\optional.hpp>

namespace bmb {

class Ide {
public:
	Ide();

	void start();

private:
	bool compile();
	void run();

	bool getParams();
	void message(const std::string & str) const;
	void headerMessage(const std::string & str) const;

private:
	std::string m_filename;
	bool m_debug;
};

} // namespace bmb

#endif


﻿#ifndef COMPILER_OBJECTS_H_
#define COMPILER_OBJECTS_H_

#include <vector>
#include <map>
#include <iostream>
#include <sstream>
#include "constants\spec.h"

namespace bmb {

typedef std::vector<Word> ListOfAdresses;

enum VarType {
	UndefType = 0,
	ByteType,
	WordType
};

template< class DataTy >
class VarTraits {
public:
	static VarType getType() {
		return VarType::UndefType;
	}
};

template<>
class VarTraits<Byte> {
public:
	static VarType getType() {
		return VarType::ByteType;
	}
};

template<>
class VarTraits<Word> {
public:
	static VarType getType() {
		return VarType::WordType;
	}
};

VarType getVarTypeByName(const std::string typeString);

class Var {
public:
	Var()
		: m_address(0),
		m_varType(VarType::UndefType) {
	}

	Var(VarType varType)
		: m_address(0),
		m_varType(varType) {
	}

	Var(Word address, VarType varType)
		: m_address(address),
		m_varType(varType) {
	}

	Word getAddress() const {
		return m_address;
	}

	VarType getVarType() const {
		return m_varType;
	}

private:
	Word			m_address;
	VarType			m_varType;
};


class Object {
public:
	Object()
		: m_instanciated(false) {
	}

	Object(const std::string & name)
		: m_name(name),
		m_instanciated(false) {
	}

	const std::string & getName() const {
		return m_name;
	}

	void setName(const std::string & name) {
		m_name = name;
	}

	void setIsInstanciated() {
		m_instanciated = true;
	}

	bool isInstanciated() {
		return m_instanciated;
	}

private:
	std::string		m_name;
	bool			m_instanciated;
};


class Referencable {
public:
	Referencable() {
	}

	Referencable(Word address)
		: m_address(address) {
	}

	Word getAddress() const {
		return m_address;
	}

	void setAddress(Word & address) {
		m_address = address;
	}

	const ListOfAdresses & getReferences() const {
		return m_references;
	}

	void addReference(Word reference) {
		m_references.push_back(reference);
	}

private:
	Word				m_address;
	ListOfAdresses		m_references;
};

class Mark : public Object,
	public Referencable {
public:
	Mark() {
	}

	Mark(const std::string & name, Word address)
		: Object(name),
		Referencable(address) {
	}

	bool operator == (const Mark & other) {
		return (other.getName() == getName()
			&& other.getAddress() == getAddress());
	}
};

class Function : public Object,
	public Referencable {
public:
	Function() {
	}

	Function(const std::string & name, Word address)
		: Object(name),
		Referencable(address) {
	}

	const std::vector<Var> & getVars() const {
		return m_vars;
	}

	void addVar(const Var & var) {
		m_vars.push_back(var);
	}

private:
	std::vector<Var> m_vars;
};

template< class CompilerTy >
class CompilerList {
public:
	typedef std::map< std::string, CompilerTy > type;
};

typedef CompilerList<Var>::type			VarsList;
typedef CompilerList<Mark>::type		MarksList;
typedef CompilerList<Function>::type	FunctionsList;

} // namespace bmb

#endif
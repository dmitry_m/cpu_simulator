#ifndef COMPILER_LIST_HOLDER_H_
#define COMPILER_LIST_HOLDER_H_

#include <string>
#include <functional>
#include <vector>
#include <map>

#include "ide\compiler\compilerObjects.h"

namespace bmb {

class CompilerListHolder {
public:
	template< class CompilerObjectTy >
	typename CompilerList<CompilerObjectTy>::type & getObjectsList() {
		static CompilerList<CompilerObjectTy>::type l_list;
		CompilerList<CompilerObjectTy>::type * ptr = &l_list;
		return l_list;
	}
};

} // namespace bmb

#endif


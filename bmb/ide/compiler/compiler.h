#ifndef COMPILER_H_
#define COMPILER_H_

#include <string>
#include <functional>
#include <vector>
#include <map>

#include "mem\valueDescriptor.h"
#include "ide\compiler\compilerObjects.h"
#include "ide\compiler\compilerListHolder.h"

namespace bmb {

class Compiler : public CompilerListHolder {
	typedef bool (Compiler::*CompilerInstruction)(std::stringstream&, std::ofstream&, Word&);
	typedef CompilerList<CompilerInstruction>::type InstructionsHandlersList;

public:
	Compiler();

	bool compile(const std::string&);

private:
	void initInstructionHandlers();

private:
	// core

	template< class ObjectTy >
	bool getObjectAddressByName(const std::string &name, Word &addr);

	template< class ObjectTy >
	bool objectExists(const std::string &name);

	template< class DataTy >
	void writeToMemory(const DataTy&, std::ofstream&, const Word&);

	template<>
	void writeToMemory<std::string>(const std::string &data, std::ofstream &out, const Word &offset);

	template< class DataTy >
	void writeToMemoryWithShift(const DataTy&, std::ofstream&, Word&);

	template<>
	void writeToMemoryWithShift<std::string>(const std::string&, std::ofstream&, Word&);

	void writeOffsetToMemoryWithShift(Word data, std::ofstream &out, Word &offset);

	template< class DataTy >
	void writeValueToMemoryWithShift(const DataTy & data, std::ofstream &out, Word &offset);

	template<>
	void writeValueToMemoryWithShift(const std::string & data, std::ofstream &out, Word &offset);
	
	void writeDebugPrefixToMemoryWithShift(std::ofstream &out, Word &offset);

	template< class DataTy >
	bool obtainValueFromStringStream(std::stringstream&, DataTy&);

	template<>
	bool obtainValueFromStringStream<Byte>(std::stringstream&, Byte&);

	template<>
	bool obtainValueFromStringStream<std::string>(std::stringstream&, std::string&);

	void obtainNameFromStringStream(std::stringstream &in, std::string &data);

	template< class ObjectTy >
	bool initObjectSpecific(std::stringstream &ss, ObjectTy & object, Word & offset);

	template<>
	bool initObjectSpecific<Function>(std::stringstream &ss, Function & object, Word & offset);

	template< class ObjectTy >
	bool callReferencableSpecific(std::stringstream &ss, ObjectTy & object);

	template< class ObjectTy >
	void replaceRefWithAddress(std::ofstream & out, const ObjectTy & ref, const Word & addr);

	template< class RefTy >
	bool replaceRefsWithAddresses(std::ofstream &out);

	bool isValidCommand(const std::string&);

	void prepareOut(std::ofstream&, Word&);

	bool restoreHandlingSequencesIteration(std::string & str);

	void restoreHandlingSequences(std::string & str);

	void deleteTabs(std::string & str);
	
	template< class DataTy >
	bool recognizeValue(std::stringstream &ss, std::ofstream &out, Word &offset);

	bool recognizeVar(std::stringstream &ss, std::ofstream &out, Word &offset);
	
	template< class DataTy >
	int getSize(const DataTy & value);

	template<>
	int getSize<std::string>(const std::string & value);

	// common handlers

	template< Byte CommandCode >
	bool LCommand(std::stringstream &ss, std::ofstream &out, Word &offset);

	bool LCommandArgument(std::stringstream &ss, std::ofstream &out, Word &offset);

	template< Byte CommandCode, int Arguments >
	bool LCommandWithWordArguments(std::stringstream &ss, std::ofstream &out, Word &offset);

	// special handlers

	bool LShutdown(std::stringstream&, std::ofstream&, Word&);

	template< class DataTy >
	bool LDefine(std::stringstream&, std::ofstream&, Word&);

	template< class DataTy >
	bool LDefineString(std::stringstream&, std::ofstream&, Word&);

	template< class ObjectTy >
	bool LDefineReferencable(std::stringstream &ss, std::ofstream &out, Word &offset);

	template< class ObjectTy, Byte CommandCode  >
	bool LCallReferencable(std::stringstream &ss, std::ofstream &out, Word &offset);





	bool checkForFunctionVariables(std::stringstream &ss);

private:


private:
	std::string m_filename;
	InstructionsHandlersList	m_instructionHandlers;
};

} // namespace bmb

#endif


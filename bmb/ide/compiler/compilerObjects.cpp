#include "ide/compiler/compilerObjects.h"

namespace bmb {

VarType getVarTypeByName(const std::string typeString) {
	if (typeString == "byte") {
		return VarType::ByteType;
	}
	else if (typeString == "word") {
		return VarType::WordType;
	}
	else {
		return VarType::UndefType;
	}
}

} // namespace bmb
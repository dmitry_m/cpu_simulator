#include <iostream>
#include <fstream>
#include <sstream>
#include <iterator>
#include <algorithm>
#include <conio.h>
#include <boost/lexical_cast.hpp>

#include "constants/spec.h"
#include "constants/errors.h"
#include "compiler.h"

namespace bmb {

Compiler::Compiler() {
	initInstructionHandlers();
}

bool Compiler::compile(const std::string &filename) {
	bool success = true;

	std::ifstream in(filename + SOURCE_FILE_EXT);

	Word offset = 0;
	std::ofstream out(filename + BINARY_FILE_EXT, std::ios::binary);
	prepareOut(out, offset);

	std::string line;
	int lineNumber = 1;
	while (std::getline(in, line)) {
		std::stringstream ls(line);
		ls >> std::ws;
		if (ls.eof()) {
			continue;
		}

		std::string word;
		ls >> word;

		if (word.substr(0, 2) == "//") {
			continue;
		}

		if (!isValidCommand(word)) {
			std::cout << lineNumber << ": " << UNDEFINED_COMMAND << " (" << word << ')' << std::endl;
			success = false;
			break;
		}

		CompilerInstruction ci = (m_instructionHandlers[word]);
		if (!ci || !(this->*ci)(ls, out, offset)) {
			std::cout << lineNumber << ": " << COMPILATION_ERROR << std::endl;
			success = false;
			break;
		}
		out.flush();
		lineNumber++;
	}

	if (!replaceRefsWithAddresses<Mark>(out)
		|| !replaceRefsWithAddresses<Function>(out)) {
		success = false;
	}

	in.close();
	out.close();

	if (!success) {
		std::cout << COMPILATION_ERROR << std::endl;
	}
	return success;
}

void Compiler::initInstructionHandlers() {
	m_instructionHandlers[CAPTION_SHUTDOWN]			= &Compiler::LShutdown;

	m_instructionHandlers[CAPTION_ERROR]			= &Compiler::LShutdown;

	m_instructionHandlers[CAPTION_MOVE8]			= &Compiler::LCommandWithWordArguments<CODE_MOVE8, 2>;
	m_instructionHandlers[CAPTION_MOVE16]			= &Compiler::LCommandWithWordArguments<CODE_MOVE16, 2>;
	m_instructionHandlers[CAPTION_MOVE_OFFS]		= &Compiler::LCommandWithWordArguments<CODE_MOVE_OFFS, 2>;
	m_instructionHandlers[CAPTION_SEND8]			= &Compiler::LCommandWithWordArguments<CODE_SEND8, 2>;
	m_instructionHandlers[CAPTION_SEND16]			= &Compiler::LCommandWithWordArguments<CODE_SEND16, 2>;

	m_instructionHandlers[CAPTION_ADD8]				= &Compiler::LCommandWithWordArguments<CODE_ADD8, 2>;
	m_instructionHandlers[CAPTION_ADD16]			= &Compiler::LCommandWithWordArguments<CODE_ADD16, 2>;
	m_instructionHandlers[CAPTION_SUB8]				= &Compiler::LCommandWithWordArguments<CODE_SUB8, 2>;
	m_instructionHandlers[CAPTION_SUB16]			= &Compiler::LCommandWithWordArguments<CODE_SUB16, 2>;

	m_instructionHandlers[CAPTION_CMP8]				= &Compiler::LCommandWithWordArguments<CODE_CMP8, 2>;
	m_instructionHandlers[CAPTION_CMP16]			= &Compiler::LCommandWithWordArguments<CODE_CMP16, 2>;
	m_instructionHandlers[CAPTION_JMP]				= &Compiler::LCallReferencable<Mark, CODE_JMP>;
	m_instructionHandlers[CAPTION_JE]				= &Compiler::LCallReferencable<Mark, CODE_JE>;
	m_instructionHandlers[CAPTION_JNE]				= &Compiler::LCallReferencable<Mark, CODE_JNE>;
	m_instructionHandlers[CAPTION_JG]				= &Compiler::LCallReferencable<Mark, CODE_JG>;
	m_instructionHandlers[CAPTION_JL]				= &Compiler::LCallReferencable<Mark, CODE_JL>;

	m_instructionHandlers[CAPTION_PUSH]				= &Compiler::LCommandWithWordArguments<CODE_PUSH, 1> ;
	m_instructionHandlers[CAPTION_POP]				= &Compiler::LCommandWithWordArguments<CODE_POP, 1> ;
	m_instructionHandlers[CAPTION_FUNCTION]			= &Compiler::LDefineReferencable<Function>;
	m_instructionHandlers[CAPTION_CALL]				= &Compiler::LCallReferencable<Function, CODE_CALL>;
	m_instructionHandlers[CAPTION_RET]				= &Compiler::LCommandWithWordArguments<CODE_RET, 0>;

	m_instructionHandlers[CAPTION_OUT8]				= &Compiler::LCommandWithWordArguments<CODE_OUT8, 1>;
	m_instructionHandlers[CAPTION_OUT16]			= &Compiler::LCommandWithWordArguments<CODE_OUT16, 1>;
	m_instructionHandlers[CAPTION_OUTS]				= &Compiler::LCommandWithWordArguments<CODE_OUTS, 1>;
	m_instructionHandlers[CAPTION_OUTS_S]			= &Compiler::LCommandWithWordArguments<CODE_OUTS_S, 2>;
	m_instructionHandlers[CAPTION_IN8]				= &Compiler::LCommandWithWordArguments<CODE_IN8, 1>;
	m_instructionHandlers[CAPTION_IN16]				= &Compiler::LCommandWithWordArguments<CODE_IN16, 1>; 

	m_instructionHandlers[CAPTION_MARK]				= &Compiler::LDefineReferencable<Mark>;
	m_instructionHandlers[CAPTION_BYTE]				= &Compiler::LDefine<Byte>;
	m_instructionHandlers[CAPTION_WORD]				= &Compiler::LDefine<Word>;
	m_instructionHandlers[CAPTION_STRING]			= &Compiler::LDefine<std::string>;
}

// core

template< class ObjectTy >
bool Compiler::getObjectAddressByName(const std::string &name, Word &addr) {
	CompilerList<ObjectTy>::type::iterator it = getObjectsList<ObjectTy>().find(name);
	if (it != getObjectsList<ObjectTy>().end()) {
		addr = getObjectsList<ObjectTy>()[name].getAddress();
		return true;
	}
	else {
		std::cout << DataError<ObjectTy>::notDefined() << " (" << name << ')' << std::endl;
		return false;
	}
}

template< class ObjectTy >
bool Compiler::objectExists(const std::string &name) {
	return (getObjectsList<ObjectTy>().find(name) != getObjectsList<ObjectTy>().end());
}

template< class DataTy >
void Compiler::writeToMemory(const DataTy &data, std::ofstream &out, const Word &offset) {
	out.seekp(offset);
	out.write(reinterpret_cast<char*>(const_cast<DataTy*>(&data)), sizeof(DataTy));
}

template<>
void Compiler::writeToMemory<std::string>(const std::string &data, std::ofstream &out, const Word &offset) {
	out.seekp(offset);
	out.write(data.c_str(), data.length());
}

template< class DataTy >
void Compiler::writeToMemoryWithShift(const DataTy &data, std::ofstream &out, Word &offset) {
	writeToMemory<DataTy>(data, out, offset);
	offset += sizeof(data);
}

template<>
void Compiler::writeToMemoryWithShift<std::string>(const std::string &data, std::ofstream &out, Word &offset) {
	writeToMemory<std::string>(data, out, offset);
	offset += data.length();
}

void Compiler::writeOffsetToMemoryWithShift(Word data, std::ofstream &out, Word &offset) {
	writeToMemoryWithShift<Byte>(CODE_OFFSET, out, offset);
	writeToMemoryWithShift<Word>(data, out, offset);
}

template< class DataTy >
void Compiler::writeValueToMemoryWithShift(const DataTy & data, std::ofstream &out, Word &offset) {
	writeToMemoryWithShift<Byte>(CODE_VALUE, out, offset);
	writeToMemoryWithShift<Word>(sizeof(DataTy), out, offset);
	writeToMemoryWithShift<DataTy>(data, out, offset);
}

template<>
void Compiler::writeValueToMemoryWithShift<std::string>(const std::string & data, std::ofstream &out, Word &offset) {
	writeToMemoryWithShift<Byte>(CODE_VALUE, out, offset);
	writeToMemoryWithShift<Word>(data.length(), out, offset);
	writeToMemoryWithShift<std::string>(data, out, offset);
}

void Compiler::writeDebugPrefixToMemoryWithShift(std::ofstream &out, Word &offset) {
	writeToMemoryWithShift<Byte>(CODE_DEBUG, out, offset);
}

template< class DataTy >
bool Compiler::obtainValueFromStringStream(std::stringstream &in, DataTy &data) {
	in >> data >> std::ws;
	return true;
}

template<>
bool Compiler::obtainValueFromStringStream<Byte>(std::stringstream &in, Byte &data) {
	Byte wordValue;
	in >> wordValue >> std::ws;
	data = wordValue;
	return true;
}

template<>
bool Compiler::obtainValueFromStringStream<std::string>(std::stringstream &in, std::string &data) {
	const char borderChar = '\"';
	
	std::string returnString;

	std::find(std::istreambuf_iterator<char>(in), std::istreambuf_iterator<char>(), borderChar);
	if (!in) {
		return false;
	}

	std::find_if(++std::istreambuf_iterator<char>(in), std::istreambuf_iterator<char>(), [borderChar, &returnString](char ch) {
		if (borderChar != ch) {
			returnString.push_back(ch);
			return false;
		}
		return true;
	});
	returnString = returnString + '\0';
	
	restoreHandlingSequences(returnString);

	data = returnString;

	return true;
}

void Compiler::obtainNameFromStringStream(std::stringstream &in, std::string &data) {
	in >> data;
}

template< class ObjectTy >
bool Compiler::initObjectSpecific(std::stringstream &ss, ObjectTy & object, Word & offset) {
	return true;
}

template<>
bool Compiler::initObjectSpecific<Function>(std::stringstream &ss, Function & object, Word & offset) {
	/*
	std::string str;
	obtainValueFromStringStream(ss, str);
	if (":" != str || ss.eof()) {
	return false;
	}

	while (!ss.eof()) {
	obtainValueFromStringStream(ss, str);
	object.addVar(Var(getVarTypeByName(str)));
	}
	*/
	return true;
}

template< class ObjectTy >
bool Compiler::callReferencableSpecific(std::stringstream &ss, ObjectTy & object) {
	return true;
}

template< class ObjectTy >
void Compiler::replaceRefWithAddress(std::ofstream & out, const ObjectTy & object, const Word & addr) {
	writeToMemory(object.getAddress(), out, addr);
}

template<class ObjectTy>
bool Compiler::replaceRefsWithAddresses(std::ofstream &out) {
	for (CompilerList<ObjectTy>::type::iterator each = getObjectsList<ObjectTy>().begin(); each != getObjectsList<ObjectTy>().end(); ++each) {
		const ListOfAdresses & refs = each->second.getReferences();
		for (ListOfAdresses::const_iterator ref = refs.cbegin(); ref != refs.cend(); ++ref) {
			replaceRefWithAddress<ObjectTy>(out, each->second, *ref);
		}
	}
	return true;
}

bool Compiler::isValidCommand(const std::string &name) {
	bool founded = false;
	std::map< std::string, CompilerInstruction >::iterator it;


	for (it = m_instructionHandlers.begin(); it != m_instructionHandlers.end(); it++) {
		if (it->first == name) {
			founded = true;
			break;
		}
	}
	return founded;
}

void Compiler::prepareOut(std::ofstream &out, Word &offset) {
	offset = StartAddress;
	out.seekp(offset);
	//out.fill(0);

	LCallReferencable<Mark, CODE_JMP>(std::stringstream("start"), out, offset);
}

bool Compiler::restoreHandlingSequencesIteration(std::string & str) {
	if (str.empty()) {
		return false;
	}

	const char slash = '\\';
	char symbols[] = { 'n', '0' };
	std::string mappedSymbols[] = { "\n", "\0" };

	for (std::string::iterator it = str.begin(); it != str.end() - 1; ++it) {
		for (int symbolNumber = 0; symbolNumber < sizeof(symbols); ++symbolNumber) {
			if (slash == it[0] && symbols[symbolNumber] == it[1]) {
				
				std::copy(mappedSymbols[symbolNumber].begin(),
					mappedSymbols[symbolNumber].end(),
					str.erase(it));

				return true;
			}
		}
	}

	return false;
}

void Compiler::restoreHandlingSequences(std::string & str) {
	if (str.empty()) {
		return;
	}

	while (restoreHandlingSequencesIteration(str));
}

void Compiler::deleteTabs(std::string & str) {
	if (str.empty()) {
		return;
	}

	for (std::string::iterator it = str.begin(); it != str.end();) {
		if ('\t' == *it) {
			it = str.erase(it);
		}
		else {
			++it;
		}
	}
}

template< class DataTy >
bool Compiler::recognizeValue(std::stringstream &ss, std::ofstream &out, Word &offset) {
	DataTy value;
	if (!obtainValueFromStringStream<DataTy>(ss, value)) {
		return false;
	}    

	writeValueToMemoryWithShift<DataTy>(value, out, offset);
	return true;
}

bool Compiler::recognizeVar(std::stringstream &ss, std::ofstream &out, Word &offset) {
	std::string name;
	ss >> name;

	if (name.empty()) {
		return false;
	}

	Word returnValue = 0;

	Word argOffset;
	if (!getObjectAddressByName<Var>(name, argOffset)) {
		return false;
	}

	writeOffsetToMemoryWithShift(argOffset, out, offset);
	return true;
}

template< class DataTy >
int Compiler::getSize(const DataTy & value) {
	return sizeof(value);
}

template<>
int Compiler::getSize<std::string>(const std::string & value) {
	return value.length();
}

/////////////////////////////////////////////////////////////////////////////////////////
// common handlers 

template< Byte CommandCode >
bool Compiler::LCommand(std::stringstream &ss, std::ofstream &out, Word &offset) {
	writeToMemoryWithShift(CommandCode, out, offset);
	return true;
}

bool Compiler::LCommandArgument(std::stringstream &ss, std::ofstream &out, Word &offset) {
	bool result = false;

	ss >> std::ws;
	char firstChar = ss.get();
	ss.unget();
	if ('0' < firstChar && '9' > firstChar) {
		result = recognizeValue<Word>(ss, out, offset);
	}
	else if ('\"' == firstChar) {
		result = recognizeValue<std::string>(ss, out, offset);
	}
	else {
		result = recognizeVar(ss, out, offset);
	}
	return result;
}

template< Byte CommandCode, int Arguments >
bool Compiler::LCommandWithWordArguments(std::stringstream &ss, std::ofstream &out, Word &offset) {
	if (!LCommand<CommandCode>(ss, out, offset)) {
		return false;
	}
	for (int i = 0; i < Arguments; ++i) {
		if (!LCommandArgument(ss, out, offset)) {
			return false;
		}
	}
	return true;
}

/////////////////////////////////////////////////////////////////////////////////////////
// special handlers 

bool Compiler::LShutdown(std::stringstream &ss, std::ofstream &out, Word &offset) {
	writeToMemoryWithShift(CODE_SHUTDOWN, out, offset);

	return true;
}

template< class DataTy >
bool Compiler::LDefine(std::stringstream &ss, std::ofstream &out, Word &offset) {
	std::string name;
	obtainNameFromStringStream(ss, name);
	
	if (!name.empty()) {
		if (objectExists<Var>(name)) {
			std::cout << DataError<Var>::alreadyDefined << " (" << name << ')' << std::endl;
			return false;
		}
	}

	DataTy value;
	obtainValueFromStringStream<DataTy>(ss, value);
	writeValueToMemoryWithShift<DataTy>(value, out, offset);
	
	if (!name.empty()) {
		getObjectsList<Var>()[name] = Var(offset - getSize(value), VarTraits<DataTy>::getType());
	}
	return true;
}

template< class ObjectTy >
bool Compiler::LDefineReferencable(std::stringstream &ss, std::ofstream &out, Word &offset) {
	std::string name;
	obtainNameFromStringStream(ss, name);

	writeDebugPrefixToMemoryWithShift(out, offset);
	writeValueToMemoryWithShift(name, out, offset);

	if (getObjectsList<ObjectTy>()[name].isInstanciated()) {
		std::cout << DataError<ObjectTy>::alreadyDefined() << " (" << name << ')' << std::endl;
		return false;
	}

	if (!initObjectSpecific(ss, getObjectsList<ObjectTy>()[name], offset)) {
		return false;
	}

	getObjectsList<ObjectTy>()[name].setName(name);
	getObjectsList<ObjectTy>()[name].setAddress(offset);
	getObjectsList<ObjectTy>()[name].setIsInstanciated();
	return true;
}

template< class ObjectTy, Byte CommandCode >
bool Compiler::LCallReferencable(std::stringstream &ss, std::ofstream &out, Word &offset) {
	std::string name;
	obtainNameFromStringStream(ss, name);

	if (!callReferencableSpecific<ObjectTy>(ss, getObjectsList<ObjectTy>()[name])) {
		return false;
	}

	writeToMemoryWithShift(CommandCode, out, offset);
	writeOffsetToMemoryWithShift(0xFFFF, out, offset);
	getObjectsList<ObjectTy>()[name].addReference(offset - sizeof(Word));

	return true;
}

} // namespace bmb


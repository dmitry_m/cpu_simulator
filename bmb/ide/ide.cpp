#include <iostream>
#include <conio.h>
#include <windows.h>
#include "ide.h"
#include "machine/machine.h"
#include "compiler/compiler.h"

namespace bmb {

Ide::Ide(void) {
}

void Ide::start() {
	while (true) {
		if (!getParams()) {
			break;
		}

		do {
			if (compile()) {
				run();
			}
		} while ('r' == _getch());
		system("cls");
	}
}

bool Ide::compile() {
	headerMessage("Compilation");

	bool result;
	Compiler compiler;
	result = compiler.compile(m_filename);	
	headerMessage(result ? "Success compilation" : "Compilation has been failed!");

	return result;
}

void Ide::run() {
	headerMessage("Execution");

	Machine machine(m_filename, m_debug);
	machine.powerOn();

	headerMessage("Press any key for continue... (\'r\' - restart)");
}

bool Ide::getParams() {	
	std::cout << "Source file name (\'d\' - default): ";
	std::cin >> m_filename;

	if (m_filename.empty()) {
		return false;
	}

	if ("d" == m_filename) {
		m_filename = "default";
	}
	
	std::cout << "Debug (y/n): ";
	char debug;	
	std::cin >> debug;
	m_debug = ('y' == debug);

	return true;
}

void Ide::message(const std::string & str) const {
	std::cout << str << std::endl;
}

void Ide::headerMessage(const std::string & str) const {
	message(str);
	message("---------------");
}

} // namespace bmb
#include "bootloader.h"

namespace bmb {

Bootloader::Bootloader(Cpu &cpu, Mem &mem)
	: m_cpu(&cpu),
	m_mem(&mem) {
}

void Bootloader::boot() {
	m_mem->load();
	m_cpu->start();
}

} // namespace bmb
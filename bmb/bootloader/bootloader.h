#ifndef BOOTLOADER_H_
#define BOOTLOADER_H_

#include "cpu/cpu.h"
#include "mem/mem.h"

namespace bmb {

class Bootloader {
public:
	Bootloader(Cpu&, Mem&);

	void boot();

private:
	Cpu* m_cpu;
	Mem* m_mem;
};

} // namespace bmb

#endif